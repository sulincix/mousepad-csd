# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# vyper <xvirusxx@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-05 06:31+0200\n"
"PO-Revision-Date: 2018-09-14 00:16+0000\n"
"Last-Translator: Nick Schermer <nick@xfce.org>\n"
"Language-Team: Romanian (http://www.transifex.com/xfce/xfce-apps/language/ro/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ro\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

#: ../mousepad/main.c:42
msgid "Do not register with the D-BUS session message bus"
msgstr "Fără înregistrare în sesiunea de mesaje D-BUS"

#: ../mousepad/main.c:43
msgid "Quit a running Mousepad instance"
msgstr "Închide o instanță pornită de Mousepad"

#: ../mousepad/main.c:45
msgid "Print version information and exit"
msgstr "Arată detalii despre versiune și ieși"

#. default application name
#: ../mousepad/main.c:70 ../mousepad.desktop.in.in.h:1
msgid "Mousepad"
msgstr "Mousepad"

#. initialize gtk+
#: ../mousepad/main.c:84
msgid "[FILES...]"
msgstr "[FIȘIERE...]"

#. no error message, the gui initialization failed
#: ../mousepad/main.c:90
msgid "Failed to open display."
msgstr "Nu s-a putut deschide displayul."

#: ../mousepad/main.c:107
msgid "The Xfce development team. All rights reserved."
msgstr "Echipa de dezvoltare Xfce. Toate drepturile sunt rezervate."

#: ../mousepad/main.c:108
#, c-format
msgid "Please report bugs to <%s>."
msgstr "Raportați problemele la <%s>."

#: ../mousepad/mousepad-dialogs.c:41
msgid "Mousepad is a fast text editor for the Xfce Desktop Environment."
msgstr "Mousepad este un editor rapid de texte pentru mediul desktop Xfce."

#: ../mousepad/mousepad-dialogs.c:46
msgid "translator-credits"
msgstr "Mișu Moldovan <dumol@xfce.org>"

#. display an error message to the user
#: ../mousepad/mousepad-dialogs.c:101
msgid "Failed to open the documentation browser"
msgstr "Nu s-a putut deschide documentația"

#. build dialog
#: ../mousepad/mousepad-dialogs.c:116
msgid "Select Tab Size"
msgstr "Selectați mărimea taburilor"

#. build the dialog
#: ../mousepad/mousepad-dialogs.c:197
msgid "Go To"
msgstr "Salt la"

#: ../mousepad/mousepad-dialogs.c:219
msgid "_Line number:"
msgstr "_Rândul numărul:"

#: ../mousepad/mousepad-dialogs.c:238
msgid "C_olumn number:"
msgstr "C_oloana numărul:"

#: ../mousepad/mousepad-dialogs.c:296
msgid "Remove all entries from the documents history?"
msgstr "Eliminați toate intrările din istoricul documentelor?"

#: ../mousepad/mousepad-dialogs.c:301
msgid "Clear Documents History"
msgstr "Șterge istoricul documentelor"

#: ../mousepad/mousepad-dialogs.c:304
msgid ""
"Clearing the documents history will permanently remove all currently listed "
"entries."
msgstr "Ștergerea istoricului documentelor va elimina toate intrările listate curent"

#: ../mousepad/mousepad-dialogs.c:335
msgid "Do you want to save the changes before closing?"
msgstr "Doriți să salvați modificările înainte de a închide?"

#: ../mousepad/mousepad-dialogs.c:336
msgid "Save Changes"
msgstr "Salvare modificări"

#: ../mousepad/mousepad-dialogs.c:337
msgid "_Don't Save"
msgstr "_Nu salva"

#. secondary text
#: ../mousepad/mousepad-dialogs.c:359
msgid "If you don't save the document, all the changes will be lost."
msgstr "Dacă nu salvați documentul, toate modificările vor fi pierdute."

#: ../mousepad/mousepad-dialogs.c:381
msgid ""
"The document has been externally modified. Do you want to continue saving?"
msgstr "Documentul a fost modificat de un alt program. Doriți să îl salvați în aceste condiții?"

#: ../mousepad/mousepad-dialogs.c:382
msgid "Externally Modified"
msgstr "Modificare externă"

#: ../mousepad/mousepad-dialogs.c:384
msgid "If you save the document, all of the external changes will be lost."
msgstr "Dacă nu salvați documentul, toate modificările externe vor fi pierdute."

#: ../mousepad/mousepad-dialogs.c:412
msgid "Do you want to save your changes before reloading?"
msgstr "Doriți să vă salvați modificările înainte de reîncărcare?"

#: ../mousepad/mousepad-dialogs.c:414
msgid "If you revert the file, all unsaved changes will be lost."
msgstr "Dacă reveniți la fișierul de pe disc, toate modificările nesalvate vor fi pierdute."

#. pack button, add signal and tooltip
#: ../mousepad/mousepad-document.c:502
msgid "Close this tab"
msgstr "Închide acest tab"

#. create an unique untitled document name
#: ../mousepad/mousepad-document.c:532
msgid "Untitled"
msgstr "Fără nume"

#. create the header
#: ../mousepad/mousepad-encoding-dialog.c:126
msgid "The document was not UTF-8 valid"
msgstr "Documentul nu era valid din punct de vedere al codării UTF-8"

#: ../mousepad/mousepad-encoding-dialog.c:127
msgid "Please select an encoding below."
msgstr "Selectați mai jos o codare."

#. encoding radio buttons
#: ../mousepad/mousepad-encoding-dialog.c:139
msgid "Default (UTF-8)"
msgstr "Implicită (UTF-8)"

#: ../mousepad/mousepad-encoding-dialog.c:145
msgid "System"
msgstr "De sistem"

#: ../mousepad/mousepad-encoding-dialog.c:152
msgid "Other:"
msgstr "Alta:"

#: ../mousepad/mousepad-encoding-dialog.c:172
msgid "Checking encodings..."
msgstr "Se verifică codarea..."

#. west european
#: ../mousepad/mousepad-encoding.c:25
msgid "Celtic"
msgstr "Celtică"

#: ../mousepad/mousepad-encoding.c:26 ../mousepad/mousepad-encoding.c:27
msgid "Greek"
msgstr "Greacă"

#: ../mousepad/mousepad-encoding.c:28
msgid "Nordic"
msgstr "Nordică"

#: ../mousepad/mousepad-encoding.c:29
msgid "South European"
msgstr "Sud-Europeană"

#: ../mousepad/mousepad-encoding.c:30 ../mousepad/mousepad-encoding.c:31
#: ../mousepad/mousepad-encoding.c:32 ../mousepad/mousepad-encoding.c:33
msgid "Western"
msgstr "Vestică"

#. east european
#: ../mousepad/mousepad-encoding.c:36 ../mousepad/mousepad-encoding.c:37
#: ../mousepad/mousepad-encoding.c:38
msgid "Baltic"
msgstr "Baltică"

#: ../mousepad/mousepad-encoding.c:39 ../mousepad/mousepad-encoding.c:40
#: ../mousepad/mousepad-encoding.c:41
msgid "Central European"
msgstr "Central-Europeană"

#: ../mousepad/mousepad-encoding.c:42 ../mousepad/mousepad-encoding.c:43
#: ../mousepad/mousepad-encoding.c:44 ../mousepad/mousepad-encoding.c:45
#: ../mousepad/mousepad-encoding.c:46
msgid "Cyrillic"
msgstr "Chirilică"

#: ../mousepad/mousepad-encoding.c:47
msgid "Cyrillic/Russian"
msgstr "Chirilică/Rusă"

#: ../mousepad/mousepad-encoding.c:48
msgid "Cyrillic/Ukrainian"
msgstr "Chirilică/Ucrainiană"

#: ../mousepad/mousepad-encoding.c:49
msgid "Romanian"
msgstr "Română"

#. middle eastern
#: ../mousepad/mousepad-encoding.c:52 ../mousepad/mousepad-encoding.c:53
#: ../mousepad/mousepad-encoding.c:54
msgid "Arabic"
msgstr "Arabă"

#: ../mousepad/mousepad-encoding.c:55 ../mousepad/mousepad-encoding.c:56
#: ../mousepad/mousepad-encoding.c:57
msgid "Hebrew"
msgstr "Ebraică"

#: ../mousepad/mousepad-encoding.c:58
msgid "Hebrew Visual"
msgstr "Ebraică Vizuală"

#. asian
#: ../mousepad/mousepad-encoding.c:61
msgid "Armenian"
msgstr "Armeană"

#: ../mousepad/mousepad-encoding.c:62
msgid "Georgian"
msgstr "Georgiană"

#: ../mousepad/mousepad-encoding.c:63
msgid "Thai"
msgstr "Thailandeză"

#: ../mousepad/mousepad-encoding.c:64 ../mousepad/mousepad-encoding.c:65
#: ../mousepad/mousepad-encoding.c:66
msgid "Turkish"
msgstr "Turcă"

#: ../mousepad/mousepad-encoding.c:67 ../mousepad/mousepad-encoding.c:68
#: ../mousepad/mousepad-encoding.c:69
msgid "Vietnamese"
msgstr "Vietnameză"

#. unicode
#: ../mousepad/mousepad-encoding.c:72 ../mousepad/mousepad-encoding.c:73
#: ../mousepad/mousepad-encoding.c:74 ../mousepad/mousepad-encoding.c:75
#: ../mousepad/mousepad-encoding.c:76 ../mousepad/mousepad-encoding.c:77
#: ../mousepad/mousepad-encoding.c:78 ../mousepad/mousepad-encoding.c:79
msgid "Unicode"
msgstr "Unicode"

#. east asian
#: ../mousepad/mousepad-encoding.c:82 ../mousepad/mousepad-encoding.c:83
#: ../mousepad/mousepad-encoding.c:84 ../mousepad/mousepad-encoding.c:85
msgid "Chinese Simplified"
msgstr "Chineză Simplificată"

#: ../mousepad/mousepad-encoding.c:86 ../mousepad/mousepad-encoding.c:87
#: ../mousepad/mousepad-encoding.c:88
msgid "Chinese Traditional"
msgstr "Chineză Tradițională"

#: ../mousepad/mousepad-encoding.c:89 ../mousepad/mousepad-encoding.c:90
#: ../mousepad/mousepad-encoding.c:91
msgid "Japanese"
msgstr "Japoneză"

#: ../mousepad/mousepad-encoding.c:92 ../mousepad/mousepad-encoding.c:93
#: ../mousepad/mousepad-encoding.c:94 ../mousepad/mousepad-encoding.c:95
msgid "Korean"
msgstr "Coreeană"

#: ../mousepad/mousepad-file.c:554
#, c-format
msgid "Invalid byte sequence in conversion input"
msgstr "Secvență nevalidă de octeți la conversia inputului"

#: ../mousepad/mousepad-file.c:878
#, c-format
msgid "The file \"%s\" you tried to reload does not exist anymore"
msgstr "Fișierul „%s” pe care ați încercat să-l reîncărcați nu mai există"

#: ../mousepad/mousepad-file.c:919
#, c-format
msgid "Failed to read the status of \"%s\""
msgstr "Nu s-a putut citi starea fișierului „%s”"

#: ../mousepad/mousepad-language-action.c:230
msgid "Plain Text"
msgstr ""

#: ../mousepad/mousepad-language-action.c:231
msgid "No filetype"
msgstr "Niciun tip de fișier"

#. setup the window properties
#: ../mousepad/mousepad-prefs-dialog.c:433 ../mousepad/mousepad-window.c:445
msgid "Preferences"
msgstr "Preferințe"

#: ../mousepad/mousepad-prefs-dialog.glade.h:1
msgid "Disabled"
msgstr "Dezactivat"

#: ../mousepad/mousepad-prefs-dialog.glade.h:2
msgid "Before"
msgstr "Înainte"

#: ../mousepad/mousepad-prefs-dialog.glade.h:3
msgid "After"
msgstr "După"

#: ../mousepad/mousepad-prefs-dialog.glade.h:4
msgid "Always"
msgstr "Mereu"

#: ../mousepad/mousepad-prefs-dialog.glade.h:5
msgid "Insert Tabs"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:6
msgid "Insert Spaces"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:7
msgid "none"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:8
#: ../mousepad/mousepad-style-scheme-action.c:193
msgid "None"
msgstr "Fără"

#: ../mousepad/mousepad-prefs-dialog.glade.h:9
msgid "Menu"
msgstr "Meniu"

#: ../mousepad/mousepad-prefs-dialog.glade.h:10
msgid "Small Toolbar"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:11
msgid "Large Toolbar"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:12
msgid "Button"
msgstr "Buton"

#: ../mousepad/mousepad-prefs-dialog.glade.h:13
msgid "Drag & Drop"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:14
msgid "Dialog"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:15
msgid "Icons Only"
msgstr "Doar iconițe"

#: ../mousepad/mousepad-prefs-dialog.glade.h:16
msgid "Text Only"
msgstr "Doar text"

#: ../mousepad/mousepad-prefs-dialog.glade.h:17
msgid "Text Below Icons"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:18
msgid "Text Beside Icons"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:19
#: ../mousepad/mousepad-window.c:472
msgid "Show line numbers"
msgstr "Arată numerele rândurilor"

#: ../mousepad/mousepad-prefs-dialog.glade.h:20
msgid "Display whitespace"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:21
msgid "Display line endings"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:22
msgid "Long line margin at column:"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:23
msgid "Highlight current line"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:24
msgid "Highlight matching brackets"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:25
msgid "Wrap long lines"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:26
msgid "<b>Display</b>"
msgstr "<b>Afișare</b>"

#: ../mousepad/mousepad-prefs-dialog.glade.h:27
msgid "Use system monospace font"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:28
msgid "<b>Font</b>"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:29
msgid "<b>Colour scheme</b>"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:30
msgid "View"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:31
msgid "Tab width:"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:32
msgid "Tab mode:"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:33
msgid "Enable automatic indentation"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:34
msgid "<b>Indentation</b>"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:35
msgid "Behaviour:"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:36
msgid "<b>Home/End Keys</b>"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:37
msgid "Editor"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:38
msgid "Show status bar"
msgstr "Arată bara de stare"

#: ../mousepad/mousepad-prefs-dialog.glade.h:39
msgid "Show full filename in title bar"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:40
msgid "Remember window size"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:41
msgid "Remember window position"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:42
msgid "Remember window state"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:43
msgid "<b>General</b>"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:44
msgid "Show toolbar"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:45
msgid "Style:"
msgstr "Stil:"

#: ../mousepad/mousepad-prefs-dialog.glade.h:46
msgid "Icon Size:"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:47
msgid "<b>Toolbar</b>"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:48
msgid "Always show tabs even with one file"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:49
msgid "Cycled notebook tab switching"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:50
msgid "<b>Notebook tabs</b>"
msgstr ""

#: ../mousepad/mousepad-prefs-dialog.glade.h:51
msgid "Window"
msgstr "Fereastră"

#. set a custom tab label
#: ../mousepad/mousepad-print.c:121
msgid "Document Settings"
msgstr "Opțiuni de document"

#: ../mousepad/mousepad-print.c:596
msgid "Page Setup"
msgstr "Configurare pagină"

#: ../mousepad/mousepad-print.c:606
msgid "_Adjust page size and orientation"
msgstr "_Ajustați mărimea paginii și orientarea"

#: ../mousepad/mousepad-print.c:616
msgid "Appearance"
msgstr "Aspect"

#: ../mousepad/mousepad-print.c:630
msgid "Print page _headers"
msgstr "_Tipărește antetele paginii"

#: ../mousepad/mousepad-print.c:637
msgid "Print _line numbers"
msgstr "Tipărește numerele _rândurilor: "

#: ../mousepad/mousepad-print.c:654
msgid "Numbering interval:"
msgstr ""

#: ../mousepad/mousepad-print.c:662
msgid ""
"The interval at which to print line numbers. For example a value of 1 will "
"print a line number on each line, a value of 2 will print a line number on "
"every other line, and so on."
msgstr ""

#: ../mousepad/mousepad-print.c:674
msgid "Enable text _wrapping"
msgstr "_Rupe rândurile"

#: ../mousepad/mousepad-print.c:681
msgid "Enable _syntax highlighting"
msgstr "Evidențiază _sintaxa"

#: ../mousepad/mousepad-print.c:693
msgid "Fonts"
msgstr "Fonturi"

#: ../mousepad/mousepad-print.c:715
msgid "Header:"
msgstr "Antete:"

#: ../mousepad/mousepad-print.c:725
msgid "Body:"
msgstr "Corp:"

#: ../mousepad/mousepad-print.c:735
msgid "Line numbers:"
msgstr "Numerotare rânduri: "

#. set dialog properties
#: ../mousepad/mousepad-replace-dialog.c:137
msgid "Replace"
msgstr "Înlocuire"

#: ../mousepad/mousepad-replace-dialog.c:144
#: ../mousepad/mousepad-replace-dialog.c:443
msgid "_Replace"
msgstr "Î_nlocuiește"

#: ../mousepad/mousepad-replace-dialog.c:162
msgid "_Search for:"
msgstr "_Caută:"

#: ../mousepad/mousepad-replace-dialog.c:183
msgid "Replace _with:"
msgstr "Înlocuiește c_u: "

#: ../mousepad/mousepad-replace-dialog.c:203
msgid "Search _direction:"
msgstr "Direcția căutării:"

#: ../mousepad/mousepad-replace-dialog.c:212
msgid "Up"
msgstr "În sus"

#: ../mousepad/mousepad-replace-dialog.c:213
msgid "Down"
msgstr "În jos"

#: ../mousepad/mousepad-replace-dialog.c:214
msgid "Both"
msgstr "Ambele"

#. case sensitive
#: ../mousepad/mousepad-replace-dialog.c:223
msgid "Case sensi_tive"
msgstr "_Majuscule semnificative"

#. match whole word
#: ../mousepad/mousepad-replace-dialog.c:230
msgid "_Match whole word"
msgstr "_Potrivește întregul cuvânt"

#: ../mousepad/mousepad-replace-dialog.c:241
msgid "Replace _all in:"
msgstr "Înlocuiește t_ot în:"

#: ../mousepad/mousepad-replace-dialog.c:249
msgid "Selection"
msgstr "Selecție"

#: ../mousepad/mousepad-replace-dialog.c:250
msgid "Document"
msgstr "Document"

#: ../mousepad/mousepad-replace-dialog.c:251
msgid "All Documents"
msgstr "Toate documentele"

#: ../mousepad/mousepad-replace-dialog.c:422
#, c-format
msgid "%d occurence"
msgid_plural "%d occurences"
msgstr[0] "o apariție"
msgstr[1] "%d apariții"
msgstr[2] "%d de apariții"

#: ../mousepad/mousepad-replace-dialog.c:443
msgid "_Replace All"
msgstr "Înlocuiește _tot"

#: ../mousepad/mousepad-search-bar.c:208
msgid "Fi_nd:"
msgstr "Ca_ută:"

#: ../mousepad/mousepad-search-bar.c:230
msgid "_Next"
msgstr "Înain_te"

#: ../mousepad/mousepad-search-bar.c:241
msgid "_Previous"
msgstr "Î_napoi"

#: ../mousepad/mousepad-search-bar.c:252
msgid "Highlight _All"
msgstr "E_vidențiază tot"

#. overflow menu item for when window is too narrow to show the tool bar item
#: ../mousepad/mousepad-search-bar.c:265 ../mousepad/mousepad-search-bar.c:276
msgid "Mat_ch Case"
msgstr "_Majuscule semnificative"

#: ../mousepad/mousepad-statusbar.c:140
msgid "Choose a filetype"
msgstr "Alegeți un tip de fișier"

#. language/filetype
#: ../mousepad/mousepad-statusbar.c:145 ../mousepad/mousepad-statusbar.c:255
msgid "Filetype: None"
msgstr "Tip de fișier: Niciunul"

#: ../mousepad/mousepad-statusbar.c:168
msgid "Toggle the overwrite mode"
msgstr "Comută suprascrierea"

#. overwrite label
#: ../mousepad/mousepad-statusbar.c:173
msgid "OVR"
msgstr "SUP"

#: ../mousepad/mousepad-statusbar.c:258
#, c-format
msgid "Filetype: %s"
msgstr "Tip de fișier: %s"

#: ../mousepad/mousepad-statusbar.c:278
#, c-format
msgid "Line: %d Column: %d Selection: %d"
msgstr "Rând: %d Coloană: %d Selecție: %d"

#: ../mousepad/mousepad-statusbar.c:280
#, c-format
msgid "Line: %d Column: %d"
msgstr "Rând: %d Coloană: %d"

#: ../mousepad/mousepad-style-scheme-action.c:194
msgid "No style scheme"
msgstr ""

#. show warning to the user
#: ../mousepad/mousepad-util.c:549
#, c-format
msgid "Unable to create base directory \"%s\". Saving to file \"%s\" will be aborted."
msgstr "Nu s-a putut crea directorul de bază „%s”. Salvarea fișierului „%s” va fi abandonat."

#. print error
#: ../mousepad/mousepad-util.c:595
#, c-format
msgid "Failed to store the preferences to \"%s\": %s"
msgstr "Nu s-au putut stoca preferințele în „%s”: %s"

#: ../mousepad/mousepad-window.c:401
msgid "_File"
msgstr "_Fișier"

#: ../mousepad/mousepad-window.c:402
msgid "_New"
msgstr "_Nou"

#: ../mousepad/mousepad-window.c:402
msgid "Create a new document"
msgstr "Creați un nou document"

#: ../mousepad/mousepad-window.c:403
msgid "New _Window"
msgstr "_Fereastră nouă"

#: ../mousepad/mousepad-window.c:403
msgid "Create a new document in a new window"
msgstr "Creați un nou document într-o fereastră nouă"

#: ../mousepad/mousepad-window.c:404
msgid "New From Te_mplate"
msgstr "N_ou din șablon"

#: ../mousepad/mousepad-window.c:405
msgid "_Open..."
msgstr "_Deschide..."

#: ../mousepad/mousepad-window.c:405
msgid "Open a file"
msgstr "Deschide un fișier"

#: ../mousepad/mousepad-window.c:406
msgid "Op_en Recent"
msgstr "Fișiere _recente"

#: ../mousepad/mousepad-window.c:407
msgid "No items found"
msgstr "Nu s-a găsit niciun element"

#: ../mousepad/mousepad-window.c:408
msgid "Clear _History"
msgstr "Șterge _istoricul"

#: ../mousepad/mousepad-window.c:408
msgid "Clear the recently used files history"
msgstr "Șterge istoricul fișierelor utilizate recent"

#: ../mousepad/mousepad-window.c:409
msgid "Save the current document"
msgstr "Salvează documentul curent"

#: ../mousepad/mousepad-window.c:410
msgid "Save _As..."
msgstr "Salveaza c_a..."

#: ../mousepad/mousepad-window.c:410
msgid "Save current document as another file"
msgstr "Salvează documentul curent ca un alt fișier"

#: ../mousepad/mousepad-window.c:411
msgid "Save A_ll"
msgstr "Salvează t_ot"

#: ../mousepad/mousepad-window.c:411
msgid "Save all document in this window"
msgstr "Salvează toate documentele din această fereastră"

#: ../mousepad/mousepad-window.c:412
msgid "Re_vert"
msgstr "Re_venire"

#: ../mousepad/mousepad-window.c:412
msgid "Revert to the saved version of the file"
msgstr "Revenire la versiunea salvată a fișierului"

#: ../mousepad/mousepad-window.c:413
msgid "_Print..."
msgstr "_Tipărire..."

#: ../mousepad/mousepad-window.c:413
msgid "Print the current document"
msgstr "Tipărește documentul curent"

#: ../mousepad/mousepad-window.c:414
msgid "_Detach Tab"
msgstr "_Detașare tab"

#: ../mousepad/mousepad-window.c:414
msgid "Move the current document to a new window"
msgstr "Mutați documentul curent într-o nouă fereastră"

#: ../mousepad/mousepad-window.c:415
msgid "Close _Tab"
msgstr "Închidere _tab"

#: ../mousepad/mousepad-window.c:415
msgid "Close the current document"
msgstr "Închideți documentul curent"

#: ../mousepad/mousepad-window.c:416
msgid "_Close Window"
msgstr "În_chidere fereastră"

#: ../mousepad/mousepad-window.c:416
msgid "Close this window"
msgstr "Închideți aceastră fereastră"

#: ../mousepad/mousepad-window.c:418
msgid "_Edit"
msgstr "_Editare"

#: ../mousepad/mousepad-window.c:419
msgid "Undo the last action"
msgstr "Anulează ultima acțiune"

#: ../mousepad/mousepad-window.c:420
msgid "Redo the last undone action"
msgstr "Refă ultima acțiune anulată"

#: ../mousepad/mousepad-window.c:421
msgid "Cut the selection"
msgstr "Taie selecția"

#: ../mousepad/mousepad-window.c:422
msgid "Copy the selection"
msgstr "Copiază selecția"

#: ../mousepad/mousepad-window.c:423
msgid "Paste the clipboard"
msgstr "Lipește din clipboard"

#: ../mousepad/mousepad-window.c:424
msgid "Paste _Special"
msgstr "Lipire _specială"

#: ../mousepad/mousepad-window.c:425
msgid "Paste from _History"
msgstr "Lipește din _istoric"

#: ../mousepad/mousepad-window.c:425
msgid "Paste from the clipboard history"
msgstr "Lipește din istoricul clipboard-ului"

#: ../mousepad/mousepad-window.c:426
msgid "Paste as _Column"
msgstr "Lipește ca o _coloană"

#: ../mousepad/mousepad-window.c:426
msgid "Paste the clipboard text into a column"
msgstr "Lipește textul din clipboard ca o coloană"

#: ../mousepad/mousepad-window.c:427
msgid "Delete the current selection"
msgstr "Șterge selecția curentă"

#: ../mousepad/mousepad-window.c:428
msgid "Select the text in the entire document"
msgstr "Selectează textul din tot documentul"

#: ../mousepad/mousepad-window.c:429
msgid "Change the selection"
msgstr "Schimbă selecția"

#: ../mousepad/mousepad-window.c:429
msgid "Change a normal selection into a column selection and vice versa"
msgstr "Schimbă selecția obișnuită într-o selecția pe coloane și invers"

#: ../mousepad/mousepad-window.c:430
msgid "Conve_rt"
msgstr ""

#: ../mousepad/mousepad-window.c:431
msgid "To _Uppercase"
msgstr ""

#: ../mousepad/mousepad-window.c:431
msgid "Change the case of the selection to uppercase"
msgstr "Schimbă literele selecției în majuscule"

#: ../mousepad/mousepad-window.c:432
msgid "To _Lowercase"
msgstr ""

#: ../mousepad/mousepad-window.c:432
msgid "Change the case of the selection to lowercase"
msgstr "Schimbă literele selecției în minuscule"

#: ../mousepad/mousepad-window.c:433
msgid "To _Title Case"
msgstr ""

#: ../mousepad/mousepad-window.c:433
msgid "Change the case of the selection to title case"
msgstr "Schimbă literele selecției în litere de titlu"

#: ../mousepad/mousepad-window.c:434
msgid "To _Opposite Case"
msgstr ""

#: ../mousepad/mousepad-window.c:434
msgid "Change the case of the selection opposite case"
msgstr "Schimbă mărimea tuturor literelor selecției"

#: ../mousepad/mousepad-window.c:435
msgid "_Tabs to Spaces"
msgstr "_Taburi -> Spații"

#: ../mousepad/mousepad-window.c:435
msgid "Convert all tabs to spaces in the selection or document"
msgstr "Convertește toate taburile în spații în selecție ori în tot documentul"

#: ../mousepad/mousepad-window.c:436
msgid "_Spaces to Tabs"
msgstr "_Spații -> Taburi"

#: ../mousepad/mousepad-window.c:436
msgid ""
"Convert all the leading spaces to tabs in the selected line(s) or document"
msgstr "Convertește toate spațiile de început de rând în taburi în selecție ori în tot documentul"

#: ../mousepad/mousepad-window.c:437
msgid "St_rip Trailing Spaces"
msgstr "_Elimină spațiile la sfârșit de linie"

#: ../mousepad/mousepad-window.c:437
msgid "Remove all the trailing spaces from the selected line(s) or document"
msgstr "Elimină toate spațiile de la sfârșit de rând în selecție ori în tot documentul"

#: ../mousepad/mousepad-window.c:438
msgid "_Transpose"
msgstr "_Transpunere"

#: ../mousepad/mousepad-window.c:438
msgid "Reverse the order of something"
msgstr "Inversează ordinea pentru un anumit conținut"

#: ../mousepad/mousepad-window.c:439
msgid "_Move Selection"
msgstr "_Mută selecția"

#: ../mousepad/mousepad-window.c:440
msgid "Line _Up"
msgstr "Un rând mai s_us"

#: ../mousepad/mousepad-window.c:440
msgid "Move the selection one line up"
msgstr "Mută selecția un rând mai sus"

#: ../mousepad/mousepad-window.c:441
msgid "Line _Down"
msgstr "Un rând mai j_os"

#: ../mousepad/mousepad-window.c:441
msgid "Move the selection one line down"
msgstr "Mută selecția un rând mai jos"

#: ../mousepad/mousepad-window.c:442
msgid "Dup_licate Line / Selection"
msgstr ""

#: ../mousepad/mousepad-window.c:442
msgid "Duplicate the current line or selection"
msgstr "Duplică rândul curent ori selecția"

#: ../mousepad/mousepad-window.c:443
msgid "_Increase Indent"
msgstr "Mărește _indentarea"

#: ../mousepad/mousepad-window.c:443
msgid "Increase the indentation of the selection or current line"
msgstr "Mărește indentarea selecției ori a rândului curent"

#: ../mousepad/mousepad-window.c:444
msgid "_Decrease Indent"
msgstr "Micșorează i_ndentarea"

#: ../mousepad/mousepad-window.c:444
msgid "Decrease the indentation of the selection or current line"
msgstr "Micșorează indentarea selecției ori a rândului curent"

#: ../mousepad/mousepad-window.c:445
msgid "Show the preferences dialog"
msgstr ""

#: ../mousepad/mousepad-window.c:447
msgid "_Search"
msgstr ""

#: ../mousepad/mousepad-window.c:448
msgid "Search for text"
msgstr "Căutare de text"

#: ../mousepad/mousepad-window.c:449
msgid "Find _Next"
msgstr "Caută înain_te"

#: ../mousepad/mousepad-window.c:449
msgid "Search forwards for the same text"
msgstr "Caută înainte pentru același text"

#: ../mousepad/mousepad-window.c:450
msgid "Find _Previous"
msgstr "Caută î_napoi"

#: ../mousepad/mousepad-window.c:450
msgid "Search backwards for the same text"
msgstr "Caută înapoi pentru același text"

#: ../mousepad/mousepad-window.c:451
msgid "Find and Rep_lace..."
msgstr "Ca_ută și înlocuiește..."

#: ../mousepad/mousepad-window.c:451
msgid "Search for and replace text"
msgstr "Caută și înlocuiește un text"

#: ../mousepad/mousepad-window.c:452
msgid "_Go to..."
msgstr "_Salt la..."

#: ../mousepad/mousepad-window.c:452
msgid "Go to a specific location in the document"
msgstr "Salt la o anumită locație din document"

#: ../mousepad/mousepad-window.c:454
msgid "_View"
msgstr "_Vizualizare"

#: ../mousepad/mousepad-window.c:455
msgid "Select F_ont..."
msgstr "Alegeți un _font..."

#: ../mousepad/mousepad-window.c:455
msgid "Change the editor font"
msgstr "Schimbați fontul editorului"

#: ../mousepad/mousepad-window.c:456
msgid "_Color Scheme"
msgstr "_Schema de culori"

#: ../mousepad/mousepad-window.c:458
msgid "_Document"
msgstr "_Document"

#: ../mousepad/mousepad-window.c:459
msgid "Line E_nding"
msgstr "_Terminatori de rând"

#: ../mousepad/mousepad-window.c:460
msgid "Tab _Size"
msgstr "Mărime _tab"

#: ../mousepad/mousepad-window.c:461
msgid "_Filetype"
msgstr "Tip de _fișier"

#: ../mousepad/mousepad-window.c:462
msgid "_Previous Tab"
msgstr "Tabul a_nterior"

#: ../mousepad/mousepad-window.c:462
msgid "Select the previous tab"
msgstr "Comută la tabul anterior"

#: ../mousepad/mousepad-window.c:463
msgid "_Next Tab"
msgstr "Anexa _următoare"

#: ../mousepad/mousepad-window.c:463
msgid "Select the next tab"
msgstr "Comută la tabul următor"

#: ../mousepad/mousepad-window.c:465
msgid "_Help"
msgstr "_Ajutor"

#: ../mousepad/mousepad-window.c:466
msgid "_Contents"
msgstr "_Conținut"

#: ../mousepad/mousepad-window.c:466
msgid "Display the Mousepad user manual"
msgstr "Arată manualul utilizatorului Mousepad"

#: ../mousepad/mousepad-window.c:467
msgid "About this application"
msgstr "Despre acest program"

#: ../mousepad/mousepad-window.c:472
msgid "Line N_umbers"
msgstr "N_umerotare de rânduri"

#: ../mousepad/mousepad-window.c:473
msgid "_Menubar"
msgstr ""

#: ../mousepad/mousepad-window.c:473
msgid "Change the visibility of the main menubar"
msgstr ""

#: ../mousepad/mousepad-window.c:474
msgid "_Toolbar"
msgstr ""

#: ../mousepad/mousepad-window.c:474
msgid "Change the visibility of the toolbar"
msgstr ""

#: ../mousepad/mousepad-window.c:475
msgid "St_atusbar"
msgstr "Bară de _stare"

#: ../mousepad/mousepad-window.c:475
msgid "Change the visibility of the statusbar"
msgstr "Schimbă vizibilitatea barei de stare"

#: ../mousepad/mousepad-window.c:476
msgid "_Fullscreen"
msgstr "Pe t_ot ecranul"

#: ../mousepad/mousepad-window.c:476 ../mousepad/mousepad-window.c:5018
msgid "Make the window fullscreen"
msgstr ""

#: ../mousepad/mousepad-window.c:477
msgid "_Auto Indent"
msgstr "_Indentare automată"

#: ../mousepad/mousepad-window.c:477
msgid "Auto indent a new line"
msgstr "Indentează automat un nou rând"

#: ../mousepad/mousepad-window.c:478
msgid "Insert _Spaces"
msgstr "Inserează _spații"

#: ../mousepad/mousepad-window.c:478
msgid "Insert spaces when the tab button is pressed"
msgstr "Inserează spații la apăsarea tastei Tab"

#: ../mousepad/mousepad-window.c:479
msgid "_Word Wrap"
msgstr "_Rupere de rânduri"

#: ../mousepad/mousepad-window.c:479
msgid "Toggle breaking lines in between words"
msgstr "Comută ruperea rândurilor"

#: ../mousepad/mousepad-window.c:480
msgid "Write Unicode _BOM"
msgstr "Scrie un _BOM Unicode"

#: ../mousepad/mousepad-window.c:480
msgid "Store the byte-order mark in the file"
msgstr "Stochează un marcaj byte-order în fișier"

#: ../mousepad/mousepad-window.c:485
msgid "Unix (_LF)"
msgstr "Unix (_LF)"

#: ../mousepad/mousepad-window.c:485
msgid "Set the line ending of the document to Unix (LF)"
msgstr "Documentul va avea terminatori de rând în stil Unix (LF)"

#: ../mousepad/mousepad-window.c:486
msgid "Mac (_CR)"
msgstr "Mac (_CR)"

#: ../mousepad/mousepad-window.c:486
msgid "Set the line ending of the document to Mac (CR)"
msgstr "Documentul va avea terminatori de rând în stil Mac (CR)"

#: ../mousepad/mousepad-window.c:487
msgid "DOS / Windows (C_R LF)"
msgstr "DOS / Windows (C_R LF)"

#: ../mousepad/mousepad-window.c:487
msgid "Set the line ending of the document to DOS / Windows (CR LF)"
msgstr "Documentul va avea terminatori de rând în stil DOS / Windows (CR LF)"

#. add the label with the root warning
#: ../mousepad/mousepad-window.c:860
msgid "Warning, you are using the root account, you may harm your system."
msgstr "Atenție, folosiți contul „root”, puteți suprascrie fișiere vitale sistemului."

#. show the warning
#: ../mousepad/mousepad-window.c:1516
msgid "Failed to open the document"
msgstr "Nu s-a putut deschide documentul"

#: ../mousepad/mousepad-window.c:1713
msgid "Read Only"
msgstr "Doar citire"

#: ../mousepad/mousepad-window.c:2382
#, c-format
msgid ""
"No template files found in\n"
"'%s'"
msgstr ""

#. create other action
#: ../mousepad/mousepad-window.c:2498
msgid "Set custom tab size"
msgstr "Mărime personalizată pentru taburi"

#. create suitable label for the other menu
#: ../mousepad/mousepad-window.c:2546
#, c-format
msgid "Ot_her (%d)..."
msgstr "Altce_va (%d)..."

#. set action label
#: ../mousepad/mousepad-window.c:2557
msgid "Ot_her..."
msgstr "Altce_va..."

#. build description
#. get the offset length: 'Encoding: '
#: ../mousepad/mousepad-window.c:2873 ../mousepad/mousepad-window.c:3100
msgid "Charset"
msgstr "Set de caractere"

#: ../mousepad/mousepad-window.c:3005
#, c-format
msgid "Open '%s'"
msgstr "Deschide „%s”"

#: ../mousepad/mousepad-window.c:3150
msgid "Failed to clear the recent history"
msgstr "Nu s-a putut curăța istoricul"

#. create an item to inform the user
#: ../mousepad/mousepad-window.c:3580
msgid "No clipboard data"
msgstr "Nu există date în clipboard"

#. set error message
#: ../mousepad/mousepad-window.c:3719
msgid "Templates should be UTF-8 valid"
msgstr "Șabloanele ar trebui să fie codate UTF-8 valid"

#. set error message
#: ../mousepad/mousepad-window.c:3727
msgid "Reading the template failed, the menu item has been removed"
msgstr "Citirea șablonului a eșuat, elementul de meniu a fost eliminat"

#. set error message
#: ../mousepad/mousepad-window.c:3732
msgid "Loading the template failed"
msgstr "Încărcarea șablonului a eșuat"

#. create new file chooser dialog
#: ../mousepad/mousepad-window.c:3757
msgid "Open File"
msgstr "Deschidere fișier"

#: ../mousepad/mousepad-window.c:3870
#, c-format
msgid ""
"Failed to open \"%s\" for reading. It will be removed from the document "
"history"
msgstr "Nu s-a putut deschide „%s” pentru citire. Va fi eliminat din istoricul documentelor"

#. show the warning and cleanup
#: ../mousepad/mousepad-window.c:3874
msgid "Failed to open file"
msgstr "Nu s-a putut deschide fișierul"

#. show the error
#: ../mousepad/mousepad-window.c:3980 ../mousepad/mousepad-window.c:4103
msgid "Failed to save the document"
msgstr "Nu s-a putut salva documentul"

#. create the dialog
#: ../mousepad/mousepad-window.c:4004
msgid "Save As"
msgstr "Salvează ca"

#. show the error
#: ../mousepad/mousepad-window.c:4195
msgid "Failed to reload the document"
msgstr "Nu s-a putut reîncărca documentul"

#. show the error
#: ../mousepad/mousepad-window.c:4222
msgid "Failed to print the document"
msgstr "Nu s-a putut tipări documentul"

#: ../mousepad/mousepad-window.c:4846
msgid "Choose Mousepad Font"
msgstr "Alegeți un font pentru Mousepad"

#: ../mousepad/mousepad-window.c:5011
msgid "Leave fullscreen mode"
msgstr ""

#: ../mousepad.desktop.in.in.h:2
msgid "Simple Text Editor"
msgstr "Un editor simplu de text"

#: ../mousepad.desktop.in.in.h:3
msgid "Text Editor"
msgstr "Editor de text"

#. SECURITY:
#. - A normal active user can run mousepad without elevated rights. They
#. may wish to modify a file they normally do not have read/write access
#. to. This isn't a good idea, but is common on single user systems.
#: ../org.xfce.mousepad.policy.in.in.h:6
msgid "Run Mousepad as root"
msgstr ""

#: ../org.xfce.mousepad.policy.in.in.h:7
msgid "Authentication is required to run Mousepad as root."
msgstr ""
